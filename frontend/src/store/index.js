import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    status: "",
    progress: 0,
    graphData: undefined
  },
  mutations: {
    graph_data_request(state) {
      state.status = "loading";
    },
    graph_data_success(state, data) {
      state.status = "success";
      state.graphData = data;
    },
    graph_data_error(state) {
      state.status = "error";
    }
  },
  actions: {
    upload({ commit }, file) {
      return new Promise((resolve, reject) => {
        commit("graph_data_request");
        let formData = new FormData();
        formData.append("file", file);
        axios
          .post("http://127.0.0.1:8081/upload", formData, {
            headers: {
              "Content-Type": "multipart/form-data"
            },
            onUploadProgress: progressEvent =>
              (this.state.progress = Math.round(
                (100 * progressEvent.loaded) / progressEvent.total
              ))
          })
          .then(response => {
            commit("graph_data_success", response.data);
            resolve(response);
          })
          .catch(error => {
            commit("graph_data_error");
            reject(error);
          });
      });
    }
  },
  modules: {}
});
