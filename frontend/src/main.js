import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import G6 from "@antv/g6";
import axios from "axios";

Vue.config.productionTip = false;
Vue.prototype["G6"] = G6;
Vue.prototype["$axios"] = axios;

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount("#app");
