package gr.nitrograph.visualization.service;

import org.junit.*;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static org.junit.Assert.assertEquals;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class UploadServiceTest {

	@Autowired
	UploadService uploadService;


	@Test
	public void testExcelFileToJson() throws Exception {
		MultipartFile file = new MockMultipartFile("file", "testExcel.xls", "application/vnd.ms-excel", new ClassPathResource("testExcel.xls").getInputStream());
		Map<String, List<Map<String, String>>> actual = uploadService.upload(file);
		String expected = "{sheet1=[{id=1, label=php_frameworks, name=laravel, cluster=1}," +
				" {id=2, label=php_frameworks, name=code_igniter, cluster=1}," +
				" {id=3, label=php_frameworks, name=symfony, cluster=1}," +
				" {id=4, label=python_framework, name=django, cluster=2}," +
				" {id=5, label=python_framework, name=flask, cluster=2}," +
				" {id=6, label=python_framework, name=pyramid, cluster=2}," +
				" {id=7, label=prg_lang, name=java, cluster=3}," +
				" {id=8, label=prg_lang, name=python, cluster=3}," +
				" {id=9, label=prg_lang, name=cpp, cluster=3}," +
				" {id=10, label=prg_lang, name=csharp, cluster=3}," +
				" {id=11, label=prg_lang, name=php, cluster=3}," +
				" {id=12, label=prg_lang, name=swift, cluster=3}," +
				" {id=13, label=prg_lang, name=javascript, cluster=3}]}";
		assertEquals(expected, actual.toString());
	}

}