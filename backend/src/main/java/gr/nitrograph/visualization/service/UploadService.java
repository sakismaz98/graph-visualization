package gr.nitrograph.visualization.service;

import gr.nitrograph.visualization.util.UploadUtil;
import org.apache.poi.ss.usermodel.*;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class UploadService {
	private final UploadUtil uploadUtil;

	public UploadService(UploadUtil uploadUtil) {
		this.uploadUtil = uploadUtil;
	}

	public Map<String, List<Map<String, String>>> upload(MultipartFile file) throws Exception {
		Path tempDir = Files.createTempDirectory("");
		File tempFile = tempDir.resolve(Objects.requireNonNull(file.getOriginalFilename())).toFile();
		file.transferTo(tempFile);

		Map<String, List<Map<String, String>>> jsonResponse = new HashMap<>();

		Workbook workbook = WorkbookFactory.create(tempFile);

		DataFormatter formatter = new DataFormatter();
		FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();

		int numberOfSheets = workbook.getNumberOfSheets();

		for (int i = 0; i < numberOfSheets; i++) {

			Sheet sheet = workbook.getSheetAt(i);

			// Get the rows of current sheet
			Supplier<Stream<Row>> rowStreamSupplier = uploadUtil.getRowStreamSupplier(sheet);

			// get the first row of the current sheet (header)
			Row headerRow = rowStreamSupplier.get().findFirst().get();

			// get every cell value of the first row as a list
			List<String> headerCells = uploadUtil.getStream(headerRow)
					.map(Cell::getStringCellValue)
					.map(String::toLowerCase)
					.collect(Collectors.toList());

			int colCount = headerCells.size();

			// convert the rows into a key value pair (json)
			List<Map<String, String>> rowStream = rowStreamSupplier.get()
					.skip(1)
					.map(row -> {

						List<String> cellList = uploadUtil.cellIteratorSupplier(colCount)
								.get()
								.map(row::getCell)
								.map((cell) -> formatter.formatCellValue(cell, evaluator))
								.collect(Collectors.toList());

						return uploadUtil.cellIteratorSupplier(colCount)
								.get()
								.collect(Collectors.toMap(headerCells::get, cellList::get, (o1, o2) -> o1, LinkedHashMap::new));
					})
					.collect(Collectors.toList());

			jsonResponse.put(sheet.getSheetName().toLowerCase(Locale.ROOT), rowStream);
		}
		workbook.close();

		return jsonResponse;
	}
}
